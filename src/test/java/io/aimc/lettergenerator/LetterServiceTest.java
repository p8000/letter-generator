package io.aimc.lettergenerator;

import io.aimc.lettergenerator.initializer.KafkaContainers;
import io.aimc.lettergenerator.model.Letter;
import io.aimc.lettergenerator.service.impl.LetterServiceImpl;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext
@ContextConfiguration(initializers = KafkaContainers.Initializer.class)
class LetterServiceTest {

    @Autowired
    private LetterServiceImpl letterService;

    @Value("${kafka.topic}")
    private String topic;

    @Value("${kafka.consumer.groupId}")
    private String groupId;

    @BeforeAll
    static void setUpKafkaContainer() {
        KafkaContainers.kafka.start();
    }

    public Map<String, Object> consumerConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaContainers.kafka.getBootstrapServers());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        return props;
    }

    @Test
    public void sendToKafkaLetterTest() {
        Letter letter = Letter.builder()
                .timestamp(Instant.now())
                .idReceiver(1000)
                .postOfficeReceiverId(1000)
                .content("testcontent")
                .sender("testsender")
                .build();
        KafkaConsumer<String, Letter> consumer = new KafkaConsumer<>(consumerConfigs());
        consumer.subscribe(singletonList(topic));

        letterService.send(letter);

        assertNotNull(consumer.poll(Duration.ofMillis(10000)));
    }

}
