package io.aimc.lettergenerator.model;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
@Builder
public class Letter {
    private UUID id;
    private Instant timestamp;
    private Integer idReceiver;
    private Integer postOfficeReceiverId;
    private String content;
    private String sender;
}