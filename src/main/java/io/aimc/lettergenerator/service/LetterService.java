package io.aimc.lettergenerator.service;

import io.aimc.lettergenerator.model.Letter;

public interface LetterService {
    void send(Letter letter);
}