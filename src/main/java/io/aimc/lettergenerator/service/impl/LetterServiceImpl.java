package io.aimc.lettergenerator.service.impl;

import io.aimc.lettergenerator.model.Letter;
import io.aimc.lettergenerator.service.LetterService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class LetterServiceImpl implements LetterService {
    private final KafkaTemplate<String, Letter> kafkaTemplate;
    @Value("${kafka.topic}")
    private String topic;

    @Override
    public void send(Letter letter) {
        kafkaTemplate.send(topic, letter);
        log.info("send letter: {}", letter);
    }
}
