package io.aimc.lettergenerator;

import io.aimc.lettergenerator.model.Letter;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.UUID;

@Component
public class LetterGenerator {
    public Letter generateLetter() {
        return Letter.builder()
                .id(UUID.randomUUID())
                .timestamp(Instant.now())
                .idReceiver((int) ((Math.random() * (10 - 1)) + 1))
                .postOfficeReceiverId((int) ((Math.random() * (10 - 1)) + 1))
                .content(RandomStringUtils.randomAlphabetic(10))
                .sender(RandomStringUtils.randomAlphabetic(10))
                .build();
    }
}
