package io.aimc.lettergenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class LetterGeneratorApplication {
    public static void main(String[] args) {
        SpringApplication.run(LetterGeneratorApplication.class, args);
    }
}
