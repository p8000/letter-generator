package io.aimc.lettergenerator;

import io.aimc.lettergenerator.service.LetterService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
@Profile("!test")
public class LetterSenderScheduler {
    private final LetterService letterService;
    private final LetterGenerator letterGenerator;

    @Scheduled(fixedDelayString = "${scheduler.fixed-delay}")
    public void post() {
        letterService.send(letterGenerator.generateLetter());
    }
}
